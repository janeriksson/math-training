package math_training;

import java.util.List;
import java.util.Map;

import math_training.persistence.MathStudentPersistence;
import math_training.question.Answer;
import math_training.question.MathQuestion;
import math_training.question.MathQuestionEnumFactory;
import math_training.student.MathStudent;

/**
 * @version 1.0
 * @since 19/10/16
 */
public class DashboardController {

  MathStudentPersistence studentPersistence;

  public DashboardController(MathStudentPersistence studentPersistence) {
    this.studentPersistence = studentPersistence;
  }

  public MathStudent addStudent(String name) {
    MathStudent mathStudent = new MathStudent(name);
    studentPersistence.addStudent(mathStudent);
    return mathStudent;
  }

  public void removeStudent(int id) {
    studentPersistence.removeStudent(id);
  }

  public void updateStudent(MathStudent mathStudent) {
    studentPersistence.updateStudent(mathStudent);
  }

  public Map<Integer, String> getStudentNames() {
    return studentPersistence.getStudentNames();
  }

  public MathStudent getStudent(int studentId) {
    return studentPersistence.getStudent(studentId);
  }

  public Map<Integer, String> getStudentByName(String name) {
    return studentPersistence.getStudentByName(name.replace("*","%"));
  }

  public MathQuestion getQuestion(int studentId, int questionId) {
    return studentPersistence.getStudent(studentId).getQuestion(questionId);
  }

  public MathQuestion addQuestion(int studentId, String questionType) {
    MathStudent localStudent = studentPersistence.getStudent(studentId);
    MathQuestion mathQuestion =  MathQuestionEnumFactory.valueOf(questionType).create();
    localStudent.newQuestion(mathQuestion);
    studentPersistence.addQuestion(localStudent,mathQuestion);
    return mathQuestion;
  }

  public List<MathQuestion> getQuestions(int studentId) {
    return studentPersistence.getStudent(studentId).getQuestionList();
  }

  public boolean supplyAnswer(int studentId, int questionId, List<String> answer) {
    MathStudent localStudent = studentPersistence.getStudent(studentId);
    boolean answerCorrect = localStudent.supplyAnswer(questionId,answer);
    studentPersistence.updateStudent(localStudent);
    return answerCorrect;
  }

  public List<Answer> getSuppliedAnswers(int studentId, int questionId) {
    return studentPersistence.getStudent(studentId).getQuestion(questionId).getSuppliedAnswers();
  }
}
