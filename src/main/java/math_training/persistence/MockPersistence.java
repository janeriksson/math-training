package math_training.persistence;

import java.util.HashMap;
import java.util.Map;

import math_training.question.MathQuestion;
import math_training.student.MathStudent;

/**
 * @version 1.0
 * @since 04/11/16
 */
public class MockPersistence implements MathStudentPersistence {

  HashMap<Integer, MathStudent> mathStudentMap;
  int studentIdCount;
  long questionCount;
  long answerCount;

  public MockPersistence() {
    mathStudentMap = new HashMap<>();
    studentIdCount = 1;
    questionCount = 1;
    answerCount = 1;
  }

  @Override
  public int addStudent(MathStudent mathStudent) {
    mathStudent.setStudentId(studentIdCount);
    studentIdCount++;
    return mathStudent.getStudentId();
  }

  @Override
  public Long addQuestion(MathStudent mathStudent, MathQuestion mathQuestion) {
    mathStudent.newQuestion(mathQuestion);
    mathQuestion.setId(questionCount);
    questionCount++;
    return mathQuestion.getId();
  }

  @Override
  public Map<Integer, String> getStudentNames() {
    return null;
  }

  @Override
  public MathStudent getStudent(int id) {
    return mathStudentMap.get(id);
  }

  @Override
  public Map<Integer, String> getStudentByName(String name) {
    return null;
  }

  @Override
  public void updateStudent(MathStudent student) {

  }

  @Override
  public void removeStudent(int id) {
    mathStudentMap.remove(id);
  }
}
