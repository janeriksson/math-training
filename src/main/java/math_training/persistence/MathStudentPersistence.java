package math_training.persistence;

import java.util.Map;

import math_training.question.MathQuestion;
import math_training.student.MathStudent;

/**
 * @version 1.0
 * @since 20/10/16
 */
public interface MathStudentPersistence {

  int addStudent(MathStudent mathStudent);

  Long addQuestion(MathStudent mathStudent, MathQuestion mathQuestion);

  Map<Integer, String> getStudentNames();

  MathStudent getStudent(int id);

  Map<Integer, String>  getStudentByName(String name);

  void updateStudent(MathStudent student);

  void removeStudent(int id);
}
