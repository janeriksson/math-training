package math_training.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import math_training.question.MathQuestion;
import math_training.student.MathStudent;

/**
 * @version 1.0
 * @since 22/10/16
 */
public class HibernatePersistence implements MathStudentPersistence {

  private EntityManagerFactory entityManagerFactory;
  private EntityManager entityManager;

  public HibernatePersistence() {
    entityManagerFactory = Persistence.createEntityManagerFactory("math_training" );
    entityManager = entityManagerFactory.createEntityManager();
  }

  @Override
  public int addStudent(MathStudent mathStudent) {
    entityManager.getTransaction().begin();
    entityManager.persist(mathStudent);
    entityManager.getTransaction().commit();
    return mathStudent.getStudentId();
  }

  @Override
  public Long addQuestion(MathStudent mathStudent, MathQuestion mathQuestion) {
    entityManager.getTransaction().begin();
    entityManager.persist(mathStudent);
    entityManager.persist(mathQuestion);
    entityManager.getTransaction().commit();
    return mathQuestion.getId();
  }

  @Override
  public Map<Integer, String> getStudentNames() {
    Query query =
        entityManager.createQuery("SELECT s.studentId, s.studentName FROM MathStudent s");
    List<Object[]> results = query.getResultList();
    Map<Integer, String> resultMap = new HashMap<>();
    for(Object[] oArr : results) {
      resultMap.put((Integer) oArr[0], (String) oArr[1]);
    }
    return resultMap;
  }

  @Override
  public Map<Integer, String>  getStudentByName(String name) {
    Query query =
        entityManager.createQuery("SELECT s.studentId, s.studentName "
                                  + "FROM MathStudent s WHERE s.studentName LIKE :name");
    query.setParameter("name",name);
    List<Object[]> results = query.getResultList();
    Map<Integer, String> resultMap = new HashMap<>();
    for(Object[] oArr : results) {
      resultMap.put((Integer) oArr[0], (String) oArr[1]);
    }
    return resultMap;
  }

  @Override
  public MathStudent getStudent(int id) {
    entityManager.getTransaction().begin();
    MathStudent mathStudent = entityManager.find(MathStudent.class, id);
    entityManager.getTransaction().commit();
    return mathStudent;
  }

  @Override
  public void updateStudent(MathStudent mathStudent) {
    entityManager.getTransaction().begin();
    entityManager.merge(mathStudent);
    entityManager.getTransaction().commit();
  }

  @Override
  public void removeStudent(int id) {
    entityManager.getTransaction().begin();
    MathStudent mathStudent = entityManager.find(MathStudent.class, id);
    entityManager.remove(mathStudent);
    entityManager.getTransaction().commit();
  }
}

