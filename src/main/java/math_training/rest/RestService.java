package math_training.rest;

/**
 * @version 1.0
 * @since 19/10/16
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.fatboyindustrial.gsonjavatime.Converters;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import math_training.DashboardController;
import math_training.question.Answer;
import math_training.question.MathQuestion;
import math_training.student.MathStudent;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 * REST service to handle frontend to backend calls.
 * Based on Spark Framework. The built in Jetty server
 * is used for web.
 */
public class RestService {

  /**
   * REST resolution is handled in this method.
   * Gson is used extensively to convert java objects to JSON objects
   * for convenience in frontend.
   *
   */
  public static void runSpark(DashboardController dashboardController) {

//    corsHeaders.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
//    corsHeaders.put("Access-Control-Allow-Origin", "*");
//    corsHeaders.put("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
//    corsHeaders.put("Access-Control-Allow-Credentials", "true");

    RestUtils.enableCORS("http://localhost:3000", "GET,PUT,POST,DELETE,OPTIONS", "Content-Type");

    // TODO correct response status
    // TODO check parameters and add graceful failures
//    Gson gson = new Gson();
    final Gson gson = Converters.registerLocalDateTime(new GsonBuilder()).create();

    get("/students", (request, response) -> {
      if(request.queryParams("name") != null && !request.queryParams("name").equals("")) {
        System.out.println(request.queryParams("name"));
        Map<Integer, String>  students = dashboardController.
            getStudentByName(request.queryParams("name"));
        response.status(200);
        return students;
      } else {
        Map<Integer, String> students = dashboardController.getStudentNames();
        response.status(200);
        return students;
      }

    }, gson::toJson);

    post("/students", (request, response) -> {
      Map<String, String> reqBodyMap = RestUtils.mapFromRequestBody(request);
      MathStudent mathStudent = dashboardController.addStudent(reqBodyMap.get("studentName"));
      response.status(201);
      return mathStudent;
    }, gson::toJson);

    get("/students/:id", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":id"), "UTF-8"));
      MathStudent mathStudent = dashboardController.getStudent(studentId);
      return gson.toJson(mathStudent);
    });

    delete("/students/:id", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":id"), "UTF-8"));
      dashboardController.removeStudent(studentId);
      return true;
    });

    put("/students/:id", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":id"), "UTF-8"));
      MathStudent mathStudent = dashboardController.getStudent(studentId);
      Map<String, String> reqBodyMap = RestUtils.mapFromRequestBody(request);
      mathStudent.setStudentName(reqBodyMap.get("studentName"));
//      System.out.println(reqBodyMap.get("studentName"));
      dashboardController.updateStudent(mathStudent);
      return true;
    });

    get("/students/:id/questions", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":id"), "UTF-8"));
      List<MathQuestion> mathQuestions = dashboardController.getQuestions(studentId);
      return gson.toJson(mathQuestions);
    });

    post("/students/:id/questions", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":id"), "UTF-8"));
      Map<String, String> reqBodyMap = RestUtils.mapFromRequestBody(request);
      MathQuestion mathQuestion = dashboardController.addQuestion(studentId,reqBodyMap.get("questionType"));
      response.status(201);

      return gson.toJson(mathQuestion);
    });

    get("/students/:studentId/questions/:questionId", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":studentId"), "UTF-8"));
      int questionId = Integer.parseInt(URLDecoder.decode(request.params(":questionId"), "UTF-8"));
      MathQuestion mathQuestion = dashboardController.getQuestion(studentId,questionId);
      return gson.toJson(mathQuestion);
    });

    post("/students/:studentId/questions/:questionId/answers", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":studentId"), "UTF-8"));
      int questionId = Integer.parseInt(URLDecoder.decode(request.params(":questionId"), "UTF-8"));
      Map<String, String> reqBodyMap = RestUtils.mapFromRequestBody(request);
      List<String> answer = Arrays.asList(reqBodyMap.get("answer").split(","));
      System.out.println("Student id" + studentId);
      System.out.println("Question id" + questionId);
      boolean result = dashboardController.supplyAnswer(studentId,questionId,answer);
      response.status(200);
      return result;
    },gson::toJson);

    get("/students/:studentId/questions/:questionId/answers", (request, response) -> {
      int studentId = Integer.parseInt(URLDecoder.decode(request.params(":studentId"), "UTF-8"));
      int questionId = Integer.parseInt(URLDecoder.decode(request.params(":questionId"), "UTF-8"));
      List<Answer> suppliedAnswers =
          dashboardController.getSuppliedAnswers(studentId,questionId);
      response.status(200);
      return suppliedAnswers;
    },gson::toJson);


  }
}
