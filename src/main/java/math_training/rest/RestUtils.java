package math_training.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import spark.Request;

import static spark.Spark.before;
import static spark.Spark.options;

/**
 * @version 1.0
 * @since 20/10/16
 */
public abstract class RestUtils {

  /**
   * Private constructor to block inheritance
   */
  private RestUtils() {}

  /**
   * This method takes the request body from a POST and converts it to
   * a map between field name and field param.
   * @param request Request from
   * @return The request body as a map with the post fields as keys.
   */
  public static Map<String, String> mapFromRequestBody(Request request) {
    String decodedBody = "";
    try {
      decodedBody = URLDecoder.decode(request.body(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    Map<String, String> reqBodyMap =
        Arrays.asList(decodedBody.split("&"))
            .stream()
              .map(str -> str.split("="))
            .filter(str -> str.length > 1)
            .collect(Collectors.toMap(str -> str[0], str -> str[1]));
    return reqBodyMap;
  }

  public static void enableCORS(final String origin, final String methods, final String headers) {

    options("/*", (request, response) -> {

      String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
      if (accessControlRequestHeaders != null) {
        response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
      }

      String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
      if (accessControlRequestMethod != null) {
        response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
      }

      return "OK";
    });

    before((request, response) -> {
      response.header("Access-Control-Allow-Origin", origin);
      response.header("Access-Control-Request-Method", methods);
      response.header("Access-Control-Allow-Headers", headers);
      // Note: this may or may not be necessary in your particular application
      response.type("application/json");
    });
  }

}

