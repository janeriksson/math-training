package math_training;

import math_training.persistence.HibernatePersistence;
import math_training.rest.RestService;

/**
 * @version 1.0
 * @since 18/10/16
 */
public class Main {
  static public void main(String[] args){
//    DashboardController dashboardController = new DashboardController(new MockStudentPersistence());
    HibernatePersistence persistece = new HibernatePersistence();
    DashboardController dashboardController = new DashboardController(persistece);
//    dashboardController.addStudent("Nisse");
//    dashboardController.addQuestion(1, "NUMBER_FRIENDS");
    RestService.runSpark(dashboardController);
  }
}
