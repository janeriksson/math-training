package math_training.student;

import org.hibernate.annotations.GenericGenerator;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import math_training.question.MathQuestion;

/**
 * @version 1.0
 * @since 19/10/16
 */

@Entity
public class MathStudent {

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<MathQuestion> questionList = new ArrayList<>();

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private int studentId;

  private String studentName;

  public MathStudent() {}

  public MathStudent(String studentName) {
    this.studentName = studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public boolean supplyAnswer(int questionId, List<String> answer) {
    MathQuestion mathQuestion = questionList.stream()
        .filter(q -> q.getId() == questionId).findFirst().get();
    return mathQuestion.supplyAnswer(answer);
  }

  public void newQuestion(MathQuestion newMathQuestion) {
    questionList.add(newMathQuestion);
  }

  public MathQuestion getQuestion(int questionId) {
    return questionList.get(questionId);
  }

  public List<MathQuestion> getQuestionList() {
    return questionList;
  }

  public String getStudentName() {
    return studentName;
  }

  public int getStudentId() {
    return studentId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }
}
