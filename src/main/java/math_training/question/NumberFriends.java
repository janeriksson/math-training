package math_training.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.Entity;

/**
 * @version 1.0
 * @since 18/10/16
 */
@Entity
public class NumberFriends extends MathQuestionLogic {

  private String name = this.getClass().getSimpleName();

  private int baseNumber = 10;

  public NumberFriends() {
    this(10);
  }

  NumberFriends(int baseNumber) {
    this.baseNumber = baseNumber;
    computeNumbers();
    blankPositions = new ArrayList<>();
    blankPositions.add(1);
    operators = new ArrayList<>();
    operators.add("+");
    operators.add("=");
  }

  public void computeNumbers() {
    numbersAsInt = new ArrayList<>();
    Random randGenerator = new Random();

    // The first number is randomly generated from 1 to baseNumber - 1 to avoid
    // 0 in the results.
    int firstFriend = randGenerator.nextInt(baseNumber - 1) + 1;
    int secondFriend = baseNumber - firstFriend;
    numbersAsInt.add(firstFriend);
    numbersAsInt.add(secondFriend);
    numbersAsInt.add(baseNumber);

    numbersAsString = new ArrayList<>();
    for(Integer integer : numbersAsInt) {
      numbersAsString.add(integer.toString());
    }
  }

  @Override
  public String getClassName() {
    return name;
  }

  @Override
  public List<String> getNumbers() {
    return numbersAsString;
  }

  @Override
  public List<String> getOperators() {
    return operators;
  }

  @Override
  public void setNumbers(List<String> numbers) {
    numbersAsString = numbers;
    numbersAsInt = new ArrayList<>();
    for(String s : numbersAsString) {
      numbersAsInt.add(Integer.parseInt(s));
    }
  }

  @Override
  public List<Integer> getBlankPostitions() {
    return blankPositions;
  }

  @Override
  public boolean checkAnswer(List<String> answer) {
    boolean isCorrect = false;
    if(numbersAsString.get(blankPositions.get(0)).equals(answer.get(0))) {
      isCorrect = true;
    }
    return isCorrect;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("NumberFriends{");
    sb.append("baseNumber=").append(baseNumber);
    sb.append(", numbersAsInt=").append(numbersAsInt);
    sb.append(", numbersAsString=").append(numbersAsString);
    sb.append(", operators=").append(operators);
    sb.append(", blankPositions=").append(blankPositions);
    sb.append('}');
    return sb.toString();
  }
}
