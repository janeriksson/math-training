package math_training.question;

import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @version 1.0
 * @since 23/10/16
 */

@Entity
public class Answer {

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private Long id;

  LocalDateTime answerDate;

  @ElementCollection
  List<String> answerStrings = new ArrayList<>();

  public Answer() {}

  public Answer(List<String> answerList) {
    this.answerDate = LocalDateTime.now();
    this.answerStrings = answerList;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getAnswerDate() {
    return answerDate;
  }

  public void setAnswerDate(LocalDateTime answerDate) {
    this.answerDate = answerDate;
  }

  public List<String> getAnswerStrings() {
    return answerStrings;
  }

  public void setAnswerStrings(List<String> answerString) {
    this.answerStrings = answerString;
  }


}
