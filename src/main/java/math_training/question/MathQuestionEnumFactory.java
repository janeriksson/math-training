package math_training.question;

/**
 * @version 1.0
 * @since 20/10/16
 */
public enum MathQuestionEnumFactory implements MathQuestionFactory{
  NUMBER_FRIENDS {
    @Override
    public MathQuestion create() {
      return new MathQuestion(new NumberFriends());
    }
  },
  TWO_TERM_ADDITION {
    @Override
    public MathQuestion create() {
      return new MathQuestion(new TwoTermAddition());
    }
  }
}
