package math_training.question;

/**
 * @version 1.0
 * @since 20/10/16
 */
public interface MathQuestionFactory {
  MathQuestion create();
}
