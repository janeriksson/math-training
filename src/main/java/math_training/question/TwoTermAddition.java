package math_training.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.Entity;

/**
 * @version 1.0
 * @since 27/10/16
 */
@Entity
public class TwoTermAddition extends MathQuestionLogic {
  private String name = this.getClass().getSimpleName();

  private int maxNumber = 20;

  public TwoTermAddition() {
    this(20);
  }

  TwoTermAddition(int maxNumber) {
    this.maxNumber = maxNumber;
    computeNumbers();
    blankPositions = new ArrayList<>();
    blankPositions.add(2);
    operators = new ArrayList<>();
    operators.add("+");
    operators.add("=");
  }

  public void computeNumbers() {
    numbersAsInt = new ArrayList<>();
    Random randGenerator = new Random();

    int firstTerm = randGenerator.nextInt(maxNumber - 1) + 1;
    int secondTerm = randGenerator.nextInt(maxNumber - firstTerm + 1);
    numbersAsInt.add(firstTerm);
    numbersAsInt.add(secondTerm);
    numbersAsInt.add(firstTerm + secondTerm);

    numbersAsString = new ArrayList<>();
    for(Integer integer : numbersAsInt) {
      numbersAsString.add(integer.toString());
    }
  }

  @Override
  public String getClassName() {
    return name;
  }

  @Override
  public List<String> getNumbers() {
    return numbersAsString;
  }

  @Override
  public List<String> getOperators() {
    return operators;
  }

  @Override
  public void setNumbers(List<String> numbers) {
    numbersAsString = numbers;
    numbersAsInt = new ArrayList<>();
    for(String s : numbersAsString) {
      numbersAsInt.add(Integer.parseInt(s));
    }
  }

  @Override
  public List<Integer> getBlankPostitions() {
    return blankPositions;
  }

  @Override
  public boolean checkAnswer(List<String> answer) {
    boolean isCorrect = false;
    if(numbersAsString.get(blankPositions.get(0)).equals(answer.get(0))) {
      isCorrect = true;
    }
    return isCorrect;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("NumberFriends{");
    sb.append("maxNumber=").append(maxNumber);
    sb.append(", numbersAsInt=").append(numbersAsInt);
    sb.append(", numbersAsString=").append(numbersAsString);
    sb.append(", operators=").append(operators);
    sb.append(", blankPositions=").append(blankPositions);
    sb.append('}');
    return sb.toString();
  }

}
