package math_training.question;

import org.hibernate.annotations.GenericGenerator;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Base type for the specific logic implementation of a math question.
 * The numbers that are part of a
 *
 * @version 1.0
 * @since 18/10/16
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class MathQuestionLogic {

  @ElementCollection
  protected List<Integer> numbersAsInt = new ArrayList<>();

  @ElementCollection
  protected List<String> numbersAsString = new ArrayList<>();

  @ElementCollection
  protected List<String> operators = new ArrayList<>();

  @ElementCollection
  protected List<Integer> blankPositions = new ArrayList<>();

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Get the numbers of the question as Strings.
   *
   * @return Question numbers as List of String.
   */

  abstract List<String> getNumbers();

  abstract List<String> getOperators();

  abstract void setNumbers(List<String> numbers);

  abstract List<Integer> getBlankPostitions();

  abstract boolean checkAnswer(List<String> answer);

  abstract String getClassName();
}
