package math_training.question;

import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * @version 1.0
 * @since 18/10/16
 */
@Entity
public class MathQuestion implements Comparable<MathQuestion> {

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  Long id;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  private MathQuestionLogic mathQuestionLogic;

  private LocalDateTime creationTime;

  private boolean correctlyAnswered;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Answer> suppliedAnswers = new ArrayList<>();

  @ElementCollection
  private List<String> questionStrings;

  public MathQuestion(MathQuestionLogic mathQuestionLogic) {
    this.mathQuestionLogic = mathQuestionLogic;
    creationTime = LocalDateTime.now();
    correctlyAnswered = false;
    setQuestionStrings();
  }

  public MathQuestion() {}

  public void setQuestionStrings() {
    List<String> questionStrings = new ArrayList<>();
    List<String> numbers = mathQuestionLogic.getNumbers();
    List<String> operators = mathQuestionLogic.getOperators();
    List<Integer> blankPositions = mathQuestionLogic.getBlankPostitions();

    for (String s : numbers) {
      questionStrings.add(s);
    }

    // Interleaving the operators. Start att pos 1. Increment two pos for each operator.
    int operatorPosition = 1;
    for (String s: operators) {
      questionStrings.add(operatorPosition,s);
      operatorPosition += 2;
    }

   this.questionStrings = questionStrings;
  }

  public List<String> getQuestionStrings() {
    setQuestionStrings();
    return questionStrings;
  }


  public LocalDateTime getCreationTime() {
    return creationTime;
  }

  public boolean isCorrectlyAnswered() {
    return correctlyAnswered;
  }

  public List<Answer> getSuppliedAnswers() {
    return suppliedAnswers;
  }

  public boolean supplyAnswer(List<String> answerList) {
    if(!correctlyAnswered) {
      correctlyAnswered = mathQuestionLogic.checkAnswer(answerList);
      Answer answer = new Answer(answerList);
      suppliedAnswers.add(answer);
    }
    return correctlyAnswered;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("MathQuestion{");
    sb.append("mathQuestionLogic=").append(mathQuestionLogic);
    sb.append(", creationTime=").append(creationTime);
    sb.append(", correctlyAnswered=").append(correctlyAnswered);
    sb.append(", suppliedAnswers=").append(suppliedAnswers);
    sb.append('}');
    return sb.toString();
  }

  @Override
  public int compareTo(MathQuestion that) {
    if (this.mathQuestionLogic.getClassName().
        compareTo(that.mathQuestionLogic.getClassName()) < 0) {
      return -1;
    } else if (this.mathQuestionLogic.getClassName().
        compareTo(that.mathQuestionLogic.getClassName()) > 0) {
      return 1;
    }

    if (this.creationTime.compareTo(that.creationTime) < 0) {
      return -1;
    } else if (this.creationTime.compareTo(that.creationTime) > 0) {
      return 1;
    }

    return 0;
  }
}
