/**
 * Created by Janne on 14/10/16.
 */
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StudentsComponent} from "./students.component";
import {StudentComponent} from "./student.component";
import {QuestionsComponent} from "./questions.component";

// import { DashboardComponent }   from './dashboard.component';
// import { HeroesComponent }      from './heroes.component';
// import { HeroDetailComponent }  from './hero-detail.component';

const routes: Routes = [
    { path: '', redirectTo: '/students', pathMatch: 'full' },
    { path: 'students',  component: StudentsComponent },
    { path: 'students/:id', component: StudentComponent },
    { path: 'questions/student/:id/:questionType', component: QuestionsComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
