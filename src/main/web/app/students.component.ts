/**
 * Created by Janne on 25/10/16.
 */
import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MathStudent} from "./math-student";
import {TrainingService} from "./training.service";

@Component({
    moduleId: module.id,
    selector: 'my-students',
    templateUrl: `students.component.html`,
    styleUrls: [ 'students.component.css' ]
})

export class StudentsComponent implements OnInit {

    students : MathStudent[];
    selectedStudent : MathStudent;

    constructor(
        private router: Router,
        private trainingService: TrainingService) {
        this.students = [];
    }

    getStudents(): void {
        this.trainingService.getStudents().then(students => this.initStudents(students));
    }

    initStudents(json : string) {
        this.students = [];
        var data = JSON.parse(json);
        for (let i in data) {
            let id = +i;
            this.students.push(new MathStudent(id, data[i]));
        }
    }

    ngOnInit(): void {
        this.getStudents();
    }

    add(name : string) {
        this.trainingService.create(name).then(() => this.getStudents());
    }

    search(name : string) {
        this.trainingService.searchStudents(name).then(students => this.initStudents(students));
    }

    delete(mathStudent : MathStudent) {
        this.trainingService.delete(mathStudent.id).then(() => this.getStudents());
    }

    onSelect(mathStudent : MathStudent): void {
        this.selectedStudent = mathStudent;
        this.gotoDetail();
    }

    gotoDetail(): void {
        this.router.navigate(['/students', this.selectedStudent.id]);
    }

}
