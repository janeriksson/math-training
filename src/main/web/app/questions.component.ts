/**
 * Created by Janne on 26/10/16.
 */
import {Component, OnInit} from '@angular/core';
import {
    Input,
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';
import {Params, ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common'

import {MathStudent} from "./math-student";
import {TrainingService} from "./training.service";
import {MathQuestion} from "./math-question";

@Component({
    moduleId: module.id,
    selector: 'my-questions',
    templateUrl: `questions.component.html`,
    styleUrls: ['questions.component.css'],
    animations: [
        trigger('questionState', [
            state('inactive', style({
                backgroundColor: '#eee',
                transform: 'scale(1)'
            })),
            state('active',   style({
                backgroundColor: '#cfd8dc',
                transform: 'scale(1.1)'
            })),
            transition('inactive => active', animate('100ms ease-in')),
            transition('active => inactive', animate('100ms ease-out'))
        ])
    ]
})

export class QuestionsComponent implements OnInit {

    student : MathStudent;
    activeQuestion : MathQuestion;
    questionType : string;

    constructor(
        private trainingService: TrainingService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.questionType = params['questionType']
            this.trainingService.getStudent(id)
                .then(respData => this.student = MathStudent.deserialise(respData));
        });
    }


    newQuestion() : void {
        // this.trainingService.newQuestion(this.student.id, "TWO_TERM_ADDITION")
        this.trainingService.newQuestion(this.student.id, "TWO_TERM_ADDITION")
            .then(respData => this.activeQuestion = MathQuestion.deserialise(respData));
    }

    answerQuestion() : void {
        this.trainingService.answerQuestion(this.student.id,
            this.activeQuestion.id, this.activeQuestion.nextAnswerStrings).then(respData => {
                this.activeQuestion.correctlyAnswered = JSON.parse(respData);

                if(this.activeQuestion.correctlyAnswered) {
                    this.trainingService.getStudent(this.student.id)
                        .then(respData => this.student = MathStudent.deserialise(respData));
                    this.newQuestion();
                } else {
                    this.activeQuestion.nextAnswerStrings =
                        new Array(this.activeQuestion.blankPositions.length);
                }
            });
    }

    goBack(): void {
        this.location.back();
    }
}
