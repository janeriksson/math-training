import {Timestamp} from "rxjs";
import {Json} from "@angular/common/src/facade/lang";
/**
 * Created by Janne on 25/10/16.
 */
// private Long id;
//
// LocalDateTime answerDate;
//
// List<String> answerStrings = new ArrayList<>();

export class Answer {
    id : number;
    answerTime : Date;
    answerStrings : string[];

    constructor(id : number, answerTime : Date, answerStrings : string[]) {
        this.id = id;
        this.answerTime = answerTime;
        this.answerStrings = answerStrings;
    }

    static deserialise(json : string) {
        var data = JSON.parse(json);
        return new Answer(data.id, new Date(data.answerTime), data.answerStrings);
    }
}
