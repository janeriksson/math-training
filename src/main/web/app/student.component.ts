/**
 * Created by Janne on 25/10/16.
 */
import {Component, OnInit} from '@angular/core';
import {MathStudent} from "./math-student";
import {TrainingService} from "./training.service";
import {Params, ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common'

@Component({
    moduleId: module.id,
    selector: 'my-student',
    templateUrl: `student.component.html`,
    styleUrls: [ 'student.component.css' ]
})

export class StudentComponent implements OnInit {

    student : MathStudent;

    constructor(
        private trainingService: TrainingService,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.trainingService.getStudent(id)
                .then(respData => this.student = MathStudent.deserialise(respData));
        });
    }

    goBack(): void {
        this.location.back();
    }

    toQuestions(): void {
        this.router.navigate(['questions/student/', this.student.id, 'TWO_TERM_ADDITION']);
    }

    save(): void {
        this.trainingService.update(this.student.id, this.student.name)
            .then(() => this.goBack());
    }

}
