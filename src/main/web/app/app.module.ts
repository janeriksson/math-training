/**
 * Created by Janne on 25/10/16.
 */
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";


import { AppComponent }   from './app.component';
// import './rxjs-extensions';
import {TrainingService} from "./training.service";
import {StudentComponent} from "./student.component";
import {StudentsComponent} from "./students.component";
import {AppRoutingModule} from "./app-routing.module";
import {QuestionsComponent} from "./questions.component";

@NgModule({
    imports:      [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        StudentComponent,
        StudentsComponent,
        QuestionsComponent
    ],
    providers: [
        TrainingService
    ],
    bootstrap:    [ AppComponent ]
})



export class AppModule { }
