/**
 * Created by Janne on 25/10/16.
 */
import {Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";

import 'rxjs/add/operator/toPromise';
import {MathStudent} from "./math-student";

@Injectable()
export class TrainingService {
    private trainingUrl = 'http://localhost:4567/students';  // URL to web api
    private headers = new Headers({'Content-Type': 'application/json'});
    // private headersPost = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    constructor(private http: Http) { }

    create(name: string): Promise<string> {
        let postParams = "studentName=" + name;
        return this.http
            .post(this.trainingUrl, postParams, {headers: this.headers})
            .toPromise()
            .then(res => res.text())
            .catch(this.handleError);
    }

    newQuestion(studentId : number, questionType: string): Promise<string> {
        let postParams = "questionType=" + questionType;
        const url = `${this.trainingUrl}/${studentId}/questions`;
        return this.http
            .post(url, postParams, {headers: this.headers})
            .toPromise()
            .then(res => res.text())
            .catch(this.handleError);
    }

    answerQuestion(studentId : number, questionId : number, answerStrings: string[]): Promise<string> {
        let postParams = "answer=" + answerStrings;
        const url = `${this.trainingUrl}/${studentId}/questions/${questionId}/answers`;
        return this.http
            .post(url, postParams, {headers: this.headers})
            .toPromise()
            .then(res => res.text())
            .catch(this.handleError);
    }

    update(id: number, newName: string): Promise<string> {
        let postParams = "studentName=" + newName;
        const url = `${this.trainingUrl}/${id}`;
        return this.http
            .put(url, postParams, {headers: this.headers})
            .toPromise()
            .then(res => res.text())
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.trainingUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    getStudents(): Promise<string> {
        // const url = `${this.trainingUrl}/students`;
        return this.http.get(this.trainingUrl)
            .toPromise()
            .then(response => response.text())
            .catch(this.handleError);
    }

    searchStudents(name : string): Promise<string> {
        let localName = encodeURI(name);
        const url = `${this.trainingUrl}?name=${localName}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.text())
            .catch(this.handleError);
    }

    getStudent(id : number): Promise<string> {
        const url = `${this.trainingUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.text())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}