import {MathQuestion} from "./math-question";
import {Answer} from "./answer";
import {forEach} from "@angular/router/src/utils/collection";
/**
 * Created by Janne on 25/10/16.
 */

export class MathStudent {
    id : number;
    name : string;
    questionList : MathQuestion[];

    constructor(id : number, name : string, questionList? : MathQuestion[]) {
        this.id = id;
        this.name = name;
        if(questionList) {
            this.questionList = questionList;
            this.questionList.reverse();
        }
    }

    static deserialise(json : string) {
        var data = JSON.parse(json);

        var rawQuestionList = data.questionList;
        var tempQuestionList = [];
        for (let entry of rawQuestionList) {
            tempQuestionList.push(MathQuestion.deserialise(JSON.stringify(entry)));
        }

        return new MathStudent(data.studentId, data.studentName, tempQuestionList);
    }
}
