import {Answer} from "./answer";
import {MathStudent} from "./math-student";
import {Json} from "@angular/common/src/facade/lang";
/**
 * Created by Janne on 25/10/16.
 */


export class MathQuestion {
    id : number;
    questionType : string;
    creationTime : Date;
    suppliedAnswers : Answer[];
    correctlyAnswered : boolean;
    displayArray : string[];
    blankPositions : number[];
    nextAnswerStrings : string[];

    constructor(id : number,
                questionType : string,
                creationTime : Date,
                correctlyAnsered : boolean,
                displayArray : string[],
                blankPositions : number[],
                suppliedAnswers : Answer[]
    ) {
        this.id = id;
        this.questionType = questionType;
        this.creationTime = creationTime;
        this.correctlyAnswered = correctlyAnsered;
        this.displayArray = displayArray;
        this.blankPositions = blankPositions;
        this.suppliedAnswers = suppliedAnswers;
        for (let index in this.blankPositions) {
            this.blankPositions[index] = this.blankPositions[index]*2;
        }
        this.nextAnswerStrings = new Array(this.blankPositions.length);
    }

    static deserialise(json : string) {
        var data = JSON.parse(json);
        var tempAnswers = [];
        for (let entry of data.suppliedAnswers) {
            tempAnswers.push(Answer.deserialise(JSON.stringify(entry)));
        }

        return new MathQuestion(
            data.id,
            data.mathQuestionLogic.name,
            new Date(data.creationTime),
            data.correctlyAnswered,
            data.questionStrings,
            data.mathQuestionLogic.blankPositions,
            tempAnswers
        );
    }
}
