# Math training application

This specific app aims to help lower grade students exercise basic math. Each
student will be identified by name. A history of questions, and all given
answers will be kept. 

The structure of the code is intended to be easy to extend with new math
problems. To begin with there will be addition and number friends (tiokamrater).

The goal of this application is to create a full stack application, defined as 
database, java backend and static web. 
It wil also conform to the higher grade reqirements for the last projecr of a Java and DB course.
[Link to course project in swedish](https://sjk15.github.io/java-och-db/slutuppgift.html)


## Design path
1. Create pure OOP solution without regard for possible requirements for 
database or REST service.

1. Add rest support using Spark framework.

1. Explore what solution to use for database. Candidates are JDBC and Hibernate.
How does design choices in the first stage impact database/persistance solution?

1. Add static web frontend. Quick and dirty solution is html5 and vanilla js.
Best case use Angular 2.

1. Test is put late because it is not part of the grading requirements. But there will
be unit tests at least.

1. Would be fun to implement some kind of oath-solution.